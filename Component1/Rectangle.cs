﻿using System;
using System.Drawing;

namespace Component1
{
    class Rectangle : shape
    {
        public void drawShape(string[] res, Graphics g, int a, int b)
        {
            int c = Convert.ToInt32(res[1]);
            int d = Convert.ToInt32(res[2]);
            Pen p = new Pen(Color.Black, 2);
            g.DrawRectangle(p, a, b, c, d);
        }
    }
}
