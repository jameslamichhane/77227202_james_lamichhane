using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Component1;

namespace TestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Test_Moveto_Right_value()
        {
            var v = new check();
            string[] result = v.getValidate("moveto 10 10", 1, 1);
            Assert.AreEqual(result[0], "moveto", result[1], "10", result[2], "10");

        }
        public void Test_Moveto_Wrong_value()
        {
            var v = new check();
            string[] result = v.getValidate("moveto 10", 1, 1);
            Assert.AreEqual(result[0], "errormoveto");

        }
        public void Test_Drawto_Right_value()
        {
            var v = new check();
            string[] result = v.getValidate("drawto 10 10", 1, 1);
            Assert.AreEqual(result[0], "drawto", result[1], "10", result[2], "10");

        }


        public void TestMethod1()
        {
        }
    }
}
